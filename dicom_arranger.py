import subprocess
import sys
import re
import tarfile
import os
import glob
from datetime import datetime
from os.path import join, abspath


REQUIRED_PACKAGES = ["validators", "requests", "pydicom", "clint"]
TAR_FILE_EXT = [".tgz", ".tar.gz"]
TEMP_DIR = "dicom_arranger-temp-{}"
DEST_DIR = "dicom_arranger-output-{}"
TEMP_TARFILE_NAME = "dicom_arranger-temp.tar.gz"


class DicomHandler(object):
    def __init__(self, path):
        self.path = path
        now = datetime.now().strftime("%Y%m%d-%H%M%S")
        self.origin_working_dir = os.getcwd()
        self.temp_dir = join(self.origin_working_dir, TEMP_DIR.format(now))
        self.dest_dir = join(self.origin_working_dir, DEST_DIR.format(now))

    def extract_content_from_path(self):
        os.mkdir(self.temp_dir)
        os.chdir(self.temp_dir)
        
        print("downloading file...")
        response = requests.get(self.path, stream=True)
        with open(TEMP_TARFILE_NAME, "wb") as f:
            total_length = int(response.headers.get("content-length"))
            for chunk in progress.bar(response.iter_content(chunk_size=1024),
                                      expected_size=(total_length/1024) + 1): 
                if chunk:
                    f.write(chunk)
                    f.flush()
        
        print("extracting dicom files...")
        data = tarfile.open(TEMP_TARFILE_NAME)
        tar_files = data.getmembers()
        for file in progress.bar(iter(tar_files),
                                 expected_size=len(tar_files)):
            data.extract(file)
        data.close()
        os.remove(TEMP_TARFILE_NAME)
        os.chdir(self.origin_working_dir)

    def arrange_dir_tree(self):
        os.chdir(self.temp_dir)
        print("arranging directory tree...")
        dicom_files = os.listdir(self.temp_dir)
        for dicom_file_path in progress.bar(iter(dicom_files),
                                            expected_size=len(dicom_files)):
            dicom_file = pydicom.read_file(dicom_file_path)
            patient = str(dicom_file.PatientName)
            study = dicom_file.StudyID
            series = str(dicom_file.SeriesNumber)
            new_file_tree = join(self.temp_dir, patient, study, series)
            os.makedirs(new_file_tree, exist_ok=True)
            os.rename(dicom_file_path, join(new_file_tree, dicom_file_path))
        os.chdir(self.origin_working_dir)
        os.rename(self.temp_dir, self.dest_dir)

    def run(self):
        self.extract_content_from_path()
        self.arrange_dir_tree()
        print("dicom files arranged successfully!")
        return self.dest_dir


class DicomLooker(object):
    def __init__(self, rootdir):
        self.rootdir = abspath(rootdir)
        self.dicom_files = glob.glob(join(self.rootdir, 
                                          "**", "*.dcm"), recursive=True)

    def get_patient_details(self):
        details = {}
        print("scanning for patients' details...")
        for dicom_file_path in progress.bar(self.dicom_files):
            dicom_file = pydicom.read_file(dicom_file_path)
            name = str(dicom_file.PatientName)
            if not details.get(name):
                age = dicom_file.PatientAge
                sex = dicom_file.PatientSex
                details[name] = [name, age, sex]
        return list(details.values())

    def ct_average_time(self):
        print("calculating CT scan average duration...")
        total_exposure_time = 0 # in ms
        num_of_series = 0
        for series in progress.bar(glob.glob(join(self.rootdir, 
                                                  "*", "*", "*"))):
            for dicom_file_name in glob.glob(join(series, "*")):
                dicom_file = pydicom.read_file(dicom_file_name)
                total_exposure_time += dicom_file.ExposureTime
            num_of_series += 1
        average_ct_duration = float(total_exposure_time) / num_of_series
        return average_ct_duration

    def num_unique_hospitals(self):
        print("collecting hospital information...")
        hospitals = []
        for dicom_file_name in progress.bar(self.dicom_files):
            dicom_file = pydicom.read_file(dicom_file_name)
            hospitals.append(dicom_file.InstitutionName)
        hospitals = list(set(hospitals))
        return hospitals

    def run(self):
        patient_list = self.get_patient_details()
        ct_average_time = self.ct_average_time()
        unique_hospital_list = self.num_unique_hospitals()
        
        print("\nList of patients and their details:\n")
        headers = ["Patient Name", "Patient Age", "Patient Sex"]
        patient_list.insert(0, headers)
        print(make_table(patient_list))
        print("\n")

        print("CT scan average time is: {} miliseconds".format(
            ct_average_time))
        print("\n")

        print("Number of unique hospitals is: {}".format(
            len(unique_hospital_list)))
        print("Those hospitals are:")
        for hospital in unique_hospital_list:
            print(hospital)


def make_table(matrix):
    s = [[str(e) for e in row] for row in matrix]
    lengths = [max(map(len, col)) for col in zip(*s)]
    fmt = '\t'.join('{{:{}}}'.format(x) for x in lengths)
    table = [fmt.format(*row) for row in s]
    return "\n".join(table)


def main(path):
    da = DicomHandler(path)
    arranged_dir = da.run()
    dl = DicomLooker(arranged_dir)
    dl.run()


def get_installed_packages():
    pip_output = subprocess.check_output([
        sys.executable, 
        "-m", 
        "pip", 
        "freeze"]).decode("utf-8")
    installed = [
        re.match(r"([^=]+)==", p)[1] for p in pip_output.splitlines()
        ]
    return installed


if __name__ == '__main__':
    installed_packages = get_installed_packages()
    not_present_packages = list(filter(
        lambda p: p not in installed_packages,
        REQUIRED_PACKAGES
        ))
    if not_present_packages:
        s = "\n".join(
            "{}. {}".format(i, npp) 
            for i, npp in enumerate(not_present_packages, 1)
            )
        print(
            """
Before running this script, please ensure you have installed the
following packages:
{}

using the command below:
>pip install <package-name>
            """.format(s))
        sys.exit(0)
    import validators
    import requests
    import pydicom
    from clint.textui import progress
    
    if len(sys.argv) != 2:
        print("Usage:\n\tpython dicom_arranger.py <tarfile-url>")
        sys.exit(0)
    valid_url = validators.url(sys.argv[1])
    if valid_url:
        if any([ext in sys.argv[1] for ext in TAR_FILE_EXT]):
            main(sys.argv[1])
    else:
        print("Please enter a valid .tar.gz file url")
